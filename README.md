# Shooju Excel Add-In Extensions

The (Shooju Excel Add-In)[http://docs.shooju.com/excel/] provides access to Shooju data from within Excel.

This project, Shooju Excel Add-In Extensions, allows developers to extend the Shooju Excel Add-In by compiling a *dll* that is loaded alongside of the Shooju Excel Add-In. This is easier than developing and deploying a new Excel Add-In and allows tighter integration with existing Shooju Excel Add-In functionality.

Typical use cases of Shooju Excel Add-In Extensions:

* Creating new UDFs that wrap existing Shooju UDFs
* Creating new UDFs unrelated to existing Shooju functionality
* Adding macros or other UI elements to the Shooju Ribbon

## Deployment

Method 1

* Copy the Shooju.ExcelAddinExtension.dll file into .\Extensions\Shooju.ExcelAddinExtension.dll, relative to the location of the Shooju Excel Add-In xll.

Method 2 *IN PROGRESS*

* Deploy the Shooju.ExcelAddinExtension.ddl file onto a web-accessible location (does not need to be publicly accessible).
* Contact Shooju Support to set up account-specific auto-deployment of this dll.

## Development Guide

### Quick-Start

1. Optionally fork this repository.
1. Clone the repository for local development.
1. Open the 'Source\ExcelAddinExtension.sln' solution.
1. Edit ExcelAddinExtension\* as needed for desired functionality.
1. Build the solution.
1. Copy the 'Bin\Shooju.ExcelAddinExtension.dll' file to the '.\Extensions\' folder relative to where the Shooju Excel Add-In xll is loaded from.
1. Restart Excel.

### Project Structure

* 'Bin\' - project output binaries
* 'Docs\' - an Excel document that demonstrates the use of sample Shooju Extensions
* 'Source\ExcelAddinExtension\' - the example Shooju Extension that contains sample custom UDF functions and ribbon functionality. Entries.cs is the entry point - this class contains definitions of custom UDFs and ribbon controls.
* 'Source\Extensions\' - the project that holds the classes used to interface Shooju Extensions and the host. Host.cs is the main entry point - it lets the extension invoke functionality of the host, if necessary.

