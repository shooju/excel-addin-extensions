﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using Shooju.ExcelExtensions;

namespace Shooju.ExcelAddinExtension
{
    public static class Entries
    {
        public static readonly Host Host;

        public static void Init()
        {
            Host.OnTableChange = (ref object[,] data) => { TableAddLookupColumn(ref data); };

            Host.RegisterArrayFunc(typeof(Entries), nameof(SJQueryIndirect));
        }

        public static object SJHelloWorld()
        {
            return "Shooju - Hello World";
        }

        public static object SJHelloWorldArg(object arg)
        {
            return $"Shooju - Hello World - {arg}";
        }

        public static object SJHelloWorldRtd(string query)
        {
            var res = Host.CallFunc("SJQueryPoint", new[] { query });
            if (res.IsLoading())
                return res;

            return $"Shooju - RTD - {Host.GetCurrentUdfLocation()} -> '{query}'  '{res}'";
        }

        public static object SJTableEx(
            string tableId,
            object[,] query,
            object[,] fields,
            object startDate,
            object endDate,
            object maxPoints,
            object maxSeries,
            string sort,
            string extraParams)
        {
            var res = Host.CallFunc("SJTable", new[] { tableId, query, fields, startDate, endDate, maxPoints, maxSeries, sort, extraParams });
            if (res.IsLoading())
                return res;

            var workbook = Host.ExcelApp.ActiveWorkbook;
            var table = workbook.FindTable(tableId);
            if (table != null)
            {
                // can't assign this type of formula from the current execution context, so queue it to execute when this UDF exits
                Host.QueueMainThread(() =>
                {
                    var data = table.DataBodyRange;
                    data[1, data.Columns.Count] = "=[val]*[Row '#]";
                });
            }

            return res;
        }

        static void TableAddLookupColumn(ref object[,] data)
        {
            var columns = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

            for (var j = 0; j < data.GetLength(1); j++)
            {
                columns.Add(data[0, j].ToString(), j);
            }

            if (!columns.TryGetValue("val", out var valColumnIndex) || !columns.TryGetValue("lookup", out var lookupColumnIndex))
                return;

            var modified = new object[data.GetLength(0), data.GetLength(1)];

            for (var i = 0; i < data.GetLength(0); i++)
            {
                for (var j = 0; j < data.GetLength(1); j++)
                {
                    modified[i, j] = data[i, j];
                }

                modified[i, lookupColumnIndex] =
                    (i == 0) ? "LookupRes" : $"=VLOOKUP({data[i, valColumnIndex]},LookupSource,2,FALSE)";
            }

            data = modified;
        }

        public static object SJQueryIndirect(
            object[,] query,
            object[,] fields,
            object maxSeries,
            object rotation,
            object autoExpand)
        {
            var queryBody = Host.CallFunc("SJQueryField", new object[] { query, "ExpressionName" });
            if (queryBody.IsLoading())
                return queryBody;

            var res = Host.CallFunc("SJQuery", new[] { queryBody, fields, null, null, 0, "", maxSeries, "", "", rotation, autoExpand });
            if (res.IsLoading())
                return res;

            return res;
        }

        public static readonly RibbonExtension Ribbon = new RibbonExtension
        {
            Name = "Extensions",
            Items =
            {
                new RibbonItem
                {
                    Name = "Extension Button 1",
                    OnClick = OnClick,
                    ImageName = "Resources.spreadsheet.png",
                    Size = RibbonItemSize.Large,
                },
                new RibbonItem
                {
                    Name = "Extension Button 2",
                    OnClick = OnClick,
                    ImageName = "Resources.accessories-dictionary-2.png",
                },
                new RibbonItem
                {
                    Name = "Extension Menu 1",
                    Type = RibbonItemType.Menu,
                    OnClick = OnClick,
                    ImageName = "Resources.system-file-manager-4.png",
                    GetContent = () => typeof(Entries).GetContent("Resources.MenuContent.txt"),
                },
            }
        };

        static void OnClick(RibbonItem item, string id, string tag)
        {
            var text = $"«{item.Name}» «{id}»";
            if (!string.IsNullOrEmpty(tag))
                text += $" «{tag}»";
            Host.CallFunc("ShowMessage", new[] { text });
        }
    }
}
