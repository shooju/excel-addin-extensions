﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shooju.ExcelExtensions
{
    public class RibbonItem
    {
        public RibbonItemType Type = RibbonItemType.Button;
        public string Name;
        public string Tip;
        public string ImageName;
        public RibbonItemSize Size = RibbonItemSize.Normal;

        public Func<bool> IsEnabled;

        public Func<string> GetContent;

        public OnClickDelegate OnClick;

        public string SystemId; // don't assign

        public override string ToString()
        {
            return $"{Name} {Type}";
        }
    }

    public delegate void OnClickDelegate(RibbonItem item, string id, string tag);
}
