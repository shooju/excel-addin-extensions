﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shooju.ExcelExtensions
{
    public class Host
    {
        public Microsoft.Office.Interop.Excel.Application ExcelApp;

        public Action<string> ShowError;

        public CallExcelDelegate CallFunc;
        public Action<Action> QueueMainThread;
        public Action<Action> ExecuteMainThread;

        public Func<string> GetCurrentUdfLocation;

        public TableChangeDelegate OnTableChange;

        public RegisterArrayFuncDelegate RegisterArrayFunc;
    }

    public delegate object CallExcelDelegate(string funcName, object[] args);
    public delegate void TableChangeDelegate(ref object[,] data);
    public delegate void RegisterArrayFuncDelegate(Type type, string funcName);
}
