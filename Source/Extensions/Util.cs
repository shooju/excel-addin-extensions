﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

using Microsoft.Office.Interop.Excel;

namespace Shooju.ExcelExtensions
{
    public static class Util
    {
        public static string GetContent(this Type type, string name)
        {
            var stream = type.Assembly.GetManifestResourceStream(type, name);
            using (var reader = new StreamReader(stream))
            {
                var res = reader.ReadToEnd();
                return res;
            }
        }

        public static bool IsLoading(this object val)
        {
            return (val is string str && str == "Loading...");
        }

        public static ListObject FindTable(this Workbook workbook, string name)
        {
            foreach (Worksheet worksheet in workbook.Worksheets)
            {
                try
                {
                    var table = worksheet.ListObjects[name];
                    return table;
                }
                catch (COMException exc)
                {
                    if ((uint)exc.ErrorCode != 0x8002000B) // DISP_E_BADINDEX
                        throw;
                }
            }

            return null;
        }

        public static ListColumn FindColumn(this ListObject table, string name)
        {
            foreach (ListColumn column in table.ListColumns)
            {
                if (column.Name == name)
                    return column;
            }

            return null;
        }
    }
}
